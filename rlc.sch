<Qucs Schematic 0.0.15>
<Properties>
  <View=0,0,870,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=rlc.dat>
  <DataDisplay=rlc.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 370 230 0 0 0 0>
  <.AC AC1 1 660 120 0 39 0 0 "const" 1 "0" 0 "50 Hz" 0 "[50 Hz]" 1 "no" 0>
  <Vac Vidya 1 140 160 18 -26 0 1 "200 V" 1 "50 Hz" 0 "0" 0 "0" 0>
  <L Madhavan 1 370 90 -26 10 0 0 "50 mH" 1 "" 0>
  <C Hrithik 1 490 90 -26 17 0 0 "100 uF" 1 "" 0 "neutral" 0>
  <R Sunil 1 250 90 -26 15 0 0 "10 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
</Components>
<Wires>
  <280 90 340 90 "" 0 0 0 "">
  <400 90 460 90 "" 0 0 0 "">
  <520 90 580 90 "" 0 0 0 "">
  <580 90 580 230 "" 0 0 0 "">
  <370 230 580 230 "" 0 0 0 "">
  <140 230 370 230 "" 0 0 0 "">
  <140 190 140 230 "" 0 0 0 "">
  <140 90 220 90 "" 0 0 0 "">
  <140 90 140 130 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
