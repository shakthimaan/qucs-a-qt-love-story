<Qucs Schematic 0.0.15>
<Properties>
  <View=0,0,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=c2.dat>
  <DataDisplay=c2.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 300 220 0 0 0 0>
  <Vac Preity 1 180 160 18 -26 0 1 "100 V" 1 "50 Hz" 0 "0" 0 "0" 0>
  <.AC AC1 1 490 80 0 39 0 0 "const" 1 "0 Hz" 0 "50 Hz" 0 "[50 Hz]" 1 "no" 0>
  <C Shah 1 300 80 -26 17 0 0 "100 uF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <180 220 300 220 "" 0 0 0 "">
  <180 190 180 220 "" 0 0 0 "">
  <420 80 420 220 "" 0 0 0 "">
  <330 80 420 80 "" 0 0 0 "">
  <180 80 180 130 "" 0 0 0 "">
  <180 80 270 80 "" 0 0 0 "">
  <300 220 420 220 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
