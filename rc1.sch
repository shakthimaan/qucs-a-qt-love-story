<Qucs Schematic 0.0.15>
<Properties>
  <View=0,0,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=rc1.dat>
  <DataDisplay=rc1.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 110 250 0 0 0 0>
  <GND * 1 320 250 0 0 0 0>
  <.AC AC1 1 540 190 0 39 0 0 "lin" 1 "0 Hz" 1 "150 Hz" 1 "500" 1 "no" 0>
  <Vac Kajol 1 110 190 18 -26 0 1 "200 V" 1 "150 Hz" 0 "0" 0 "0" 0>
  <C Salman 1 220 110 -26 -57 0 2 "79.5 uF" 1 "" 0 "neutral" 0>
  <R Shekhar 1 320 190 15 -26 0 1 "30 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
</Components>
<Wires>
  <110 220 110 250 "" 0 0 0 "">
  <320 220 320 250 "" 0 0 0 "">
  <110 110 110 160 "" 0 0 0 "">
  <110 110 190 110 "" 0 0 0 "">
  <320 110 320 160 "" 0 0 0 "">
  <250 110 320 110 "voltage" 360 70 53 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
