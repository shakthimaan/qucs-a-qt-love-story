<Qucs Schematic 0.0.15>
<Properties>
  <View=0,40,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=l1.dat>
  <DataDisplay=l1.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.AC AC1 1 490 130 0 39 0 0 "const" 1 "0 Hz" 0 "60 Hz" 0 "[60 Hz]" 1 "no" 0>
  <GND * 1 280 230 0 0 0 0>
  <Vac Vidya 1 160 160 18 -26 0 1 "10 V" 1 "60 Hz" 0 "0" 0 "0" 0>
  <L Madhavan 1 290 80 -26 10 0 0 "10 mH" 1 "" 0>
</Components>
<Wires>
  <400 80 400 230 "" 0 0 0 "">
  <320 80 400 80 "" 0 0 0 "">
  <280 230 400 230 "" 0 0 0 "">
  <160 230 280 230 "" 0 0 0 "">
  <160 190 160 230 "" 0 0 0 "">
  <160 80 260 80 "" 0 0 0 "">
  <160 80 160 130 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
