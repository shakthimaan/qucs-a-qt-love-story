<Qucs Schematic 0.0.15>
<Properties>
  <View=0,0,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=l3.dat>
  <DataDisplay=l3.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.AC AC1 1 490 140 0 39 0 0 "const" 1 "0 Hz" 0 "60 Hz" 0 "[60 Hz]" 1 "no" 0>
  <GND * 1 190 250 0 0 0 0>
  <Vac Vidya 1 120 180 18 -26 0 1 "10 V" 1 "60 Hz" 0 "0" 0 "0" 0>
  <L Madhavan 1 240 180 10 -26 0 1 "10 mH" 1 "" 0>
  <L Surya 1 360 180 10 -26 0 1 "10 mH" 1 "" 0>
</Components>
<Wires>
  <120 250 190 250 "" 0 0 0 "">
  <120 210 120 250 "" 0 0 0 "">
  <120 100 120 150 "" 0 0 0 "">
  <120 100 240 100 "" 0 0 0 "">
  <240 100 240 150 "" 0 0 0 "">
  <240 210 240 250 "" 0 0 0 "">
  <240 250 360 250 "" 0 0 0 "">
  <240 100 360 100 "" 0 0 0 "">
  <360 100 360 150 "" 0 0 0 "">
  <360 210 360 250 "" 0 0 0 "">
  <190 250 240 250 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
