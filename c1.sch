<Qucs Schematic 0.0.15>
<Properties>
  <View=0,-60,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=c1.dat>
  <DataDisplay=c1.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 240 150 0 0 0 0>
  <.DC DC1 1 420 30 0 39 0 0 "26.85" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "no" 0 "150" 0 "no" 0 "none" 0 "CroutLU" 0>
  <C Reena 1 250 30 -26 17 0 0 "1 pF" 1 "" 0 "neutral" 0>
  <Vdc John 1 120 100 18 -26 0 1 "100 V" 1>
</Components>
<Wires>
  <120 150 240 150 "" 0 0 0 "">
  <120 130 120 150 "" 0 0 0 "">
  <240 150 360 150 "" 0 0 0 "">
  <360 30 360 150 "" 0 0 0 "">
  <280 30 360 30 "" 0 0 0 "">
  <120 30 120 70 "" 0 0 0 "">
  <120 30 220 30 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
