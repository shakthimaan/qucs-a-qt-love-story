<Qucs Schematic 0.0.15>
<Properties>
  <View=0,0,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=r2.dat>
  <DataDisplay=r2.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.DC DC1 1 390 50 0 39 0 0 "26.85" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "no" 0 "150" 0 "no" 0 "none" 0 "CroutLU" 0>
  <GND * 1 200 180 0 0 0 0>
  <R Madhuri 1 270 40 -26 15 0 0 "50 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <Vdc Amir 1 40 100 18 -26 0 1 "100 V" 1>
  <R Juhi 1 170 40 -26 15 0 0 "50 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
</Components>
<Wires>
  <40 180 200 180 "" 0 0 0 "">
  <40 130 40 180 "" 0 0 0 "">
  <200 40 240 40 "" 0 0 0 "">
  <40 40 40 70 "" 0 0 0 "">
  <40 40 140 40 "" 0 0 0 "">
  <300 40 360 40 "" 0 0 0 "">
  <360 40 360 180 "" 0 0 0 "">
  <200 180 360 180 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
