<Qucs Schematic 0.0.15>
<Properties>
  <View=0,0,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=c4.dat>
  <DataDisplay=c4.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 230 220 0 0 0 0>
  <.AC AC1 1 530 80 0 39 0 0 "const" 1 "1 GHz" 0 "10 GHz" 0 "[50 Hz]" 1 "no" 0>
  <Vac Preity 1 160 150 18 -26 0 1 "100 V" 1 "50 Hz" 0 "0" 0 "0" 0>
  <C Hrithik 1 400 150 17 -26 0 1 "100 uF" 1 "" 0 "neutral" 0>
  <C Shah 1 280 150 17 -26 0 1 "100 uF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <160 220 230 220 "" 0 0 0 "">
  <160 180 160 220 "" 0 0 0 "">
  <160 80 160 120 "" 0 0 0 "">
  <160 80 280 80 "" 0 0 0 "">
  <280 80 280 120 "" 0 0 0 "">
  <280 180 280 220 "" 0 0 0 "">
  <230 220 280 220 "" 0 0 0 "">
  <280 80 400 80 "" 0 0 0 "">
  <400 80 400 120 "" 0 0 0 "">
  <280 220 400 220 "" 0 0 0 "">
  <400 180 400 220 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
