<Qucs Schematic 0.0.15>
<Properties>
  <View=0,0,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=r3.dat>
  <DataDisplay=r3.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 120 200 0 0 0 0>
  <.DC DC1 1 430 40 0 39 0 0 "26.85" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "no" 0 "150" 0 "no" 0 "none" 0 "CroutLU" 0>
  <Vdc Amir 1 40 110 18 -26 0 1 "100 V" 1>
  <R Madhuri 1 320 110 15 -26 0 1 "50 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <R Juhi 1 180 110 15 -26 0 1 "50 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
</Components>
<Wires>
  <40 140 40 190 "" 0 0 0 "">
  <40 190 120 190 "" 0 0 0 "">
  <40 40 40 80 "" 0 0 0 "">
  <40 40 180 40 "" 0 0 0 "">
  <320 40 320 80 "" 0 0 0 "">
  <320 140 320 190 "" 0 0 0 "">
  <180 190 320 190 "" 0 0 0 "">
  <180 140 180 190 "" 0 0 0 "">
  <180 40 320 40 "" 0 0 0 "">
  <180 40 180 80 "" 0 0 0 "">
  <120 190 180 190 "" 0 0 0 "">
  <120 190 120 200 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
