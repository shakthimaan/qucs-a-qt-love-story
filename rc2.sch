<Qucs Schematic 0.0.15>
<Properties>
  <View=0,-159,800,641,1,0,0>
  <Grid=10,10,1>
  <DataSet=rc2.dat>
  <DataDisplay=rc2.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 110 90 0 0 0 0>
  <GND * 1 320 90 0 0 0 0>
  <Vac Kajol 1 110 30 18 -26 0 1 "200 V" 1 "50 Hz" 0 "0" 0 "0" 0>
  <.AC AC1 1 500 -20 0 39 0 0 "lin" 1 "0 Hz" 1 "50 Hz" 1 "50" 1 "no" 0>
  <R Shah 1 220 -50 -26 15 0 0 "30 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <C Amitabh 1 320 30 17 -26 0 1 "79.5 uF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <110 -50 190 -50 "" 0 0 0 "">
  <110 -50 110 0 "" 0 0 0 "">
  <110 60 110 90 "" 0 0 0 "">
  <250 -50 320 -50 "voltage" 270 -90 53 "">
  <320 -50 320 0 "" 0 0 0 "">
  <320 60 320 90 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
