<Qucs Schematic 0.0.15>
<Properties>
  <View=0,0,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=c3.dat>
  <DataDisplay=c3.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.AC AC1 1 490 80 0 39 0 0 "const" 1 "1 GHz" 0 "10 GHz" 0 "[50 Hz]" 1 "no" 0>
  <GND * 1 320 220 0 0 0 0>
  <C Hrithik 1 380 80 -26 17 0 0 "100 uF" 1 "" 0 "neutral" 0>
  <Vac Preity 1 180 180 18 -26 0 1 "100 V" 1 "50 Hz" 0 "0" 0 "0" 0>
  <C Shah 1 260 80 -26 17 0 0 "100 uF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <320 220 460 220 "" 0 0 0 "">
  <180 220 320 220 "" 0 0 0 "">
  <180 210 180 220 "" 0 0 0 "">
  <180 80 180 150 "" 0 0 0 "">
  <180 80 230 80 "" 0 0 0 "">
  <460 80 460 220 "" 0 0 0 "">
  <410 80 460 80 "" 0 0 0 "">
  <290 80 350 80 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
