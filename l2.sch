<Qucs Schematic 0.0.15>
<Properties>
  <View=0,0,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=l2.dat>
  <DataDisplay=l2.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.AC AC1 1 420 120 0 39 0 0 "const" 1 "0 Hz" 0 "60 Hz" 0 "[60 Hz]" 1 "no" 0>
  <GND * 1 230 220 0 0 0 0>
  <Vac Vidya 1 90 150 18 -26 0 1 "10 V" 1 "60 Hz" 0 "0" 0 "0" 0>
  <L Madhavan 1 180 70 -26 10 0 0 "10 mH" 1 "" 0>
  <L Surya 1 280 70 -26 10 0 0 "10 mH" 1 "" 0>
</Components>
<Wires>
  <310 70 360 70 "" 0 0 0 "">
  <360 70 360 220 "" 0 0 0 "">
  <210 70 250 70 "" 0 0 0 "">
  <90 220 230 220 "" 0 0 0 "">
  <90 180 90 220 "" 0 0 0 "">
  <90 70 150 70 "" 0 0 0 "">
  <90 70 90 120 "" 0 0 0 "">
  <230 220 360 220 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
